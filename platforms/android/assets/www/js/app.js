// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'blade' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'blade.controllers' is found in controllers.js
angular.module('blade', ['ionic', 'ngCordova', 'ngResource', 'ngStorage', 'pascalprecht.translate', 'angular-jwt','wu.masonry'])

    .run(function ($ionicPlatform, $rootScope, $ionicLoading, $state) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            //local notification
            //triggered when a notification was clicked outside the app (background)
            cordova.plugins.notification.local.on('click', function (notification) {
                $rootScope.$broadcast("$cordovaLocalNotification:clicked", notification);
            });
        });

        $rootScope.$on("$cordovaLocalNotification:clicked", function (event, notification) {
            console.log("$cordovaLocalNotification clicked");
            console.log(JSON.stringify(notification));
            alert(JSON.stringify(notification));
            //Internally code do the string here so we have to convert it json object
            //various ways rather keep the string at server to make sure that we have to convert that in JSON
            //followed both the path and is good...any of it.
            var dataToPass = angular.fromJson(notification.data);
            console.log("moving to " + (dataToPass.type != "ON_MOBILE_CHANGE"));
            //$rootScope.updateNotification(notification.id);
            if (dataToPass.type != "ON_MOBILE_CHANGE") {
                // $state.go("app.disputedetails",{'id':dataToPass.id});
            }
        });
        //register listeners for loading
        $rootScope.$on('loading:show', function () {
            $ionicLoading.show();
        })
        $rootScope.$on('loading:hide', function () {
            $ionicLoading.hide();
        })
    })
    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider, $httpProvider, jwtInterceptorProvider) {
        $httpProvider.interceptors.push(function ($rootScope,$injector) {
            return {
                request: function (config) {
                    $rootScope.$broadcast('loading:show');
                    return config;
                },
                response: function (response) {
                    $rootScope.$broadcast('loading:hide');
                    return response;
                },
                responseError: function (rejection) {
                    $rootScope.$broadcast('loading:hide');
                    console.log(JSON.stringify(rejection));
                    if (rejection.data === null) {

                    } else if (rejection.status === 401) {
                        $injector.invoke(function ($state) {
                            console.log($state.current.name);
                            $state.go('blade.signIn');
                        });
                        } else if (rejection.status === 403) {

                    } else if (rejection.status === 404) {

                    } else if (rejection.status === 500) {

                    }
                }
            }
        });

        //Enable cross domain calls
        $httpProvider.defaults.useXDomain = true;
        jwtInterceptorProvider.authPrefix = '';
        jwtInterceptorProvider.tokenGetter = ['config','jwtHelper','$localStorage', function (config,jwtHelper,$localStorage) {
            var theUserToken = $localStorage.userToken;
            if(theUserToken instanceof Object){
                var date = jwtHelper.getTokenExpirationDate(theUserToken.token);
                var bool = jwtHelper.isTokenExpired(theUserToken.token);
                return theUserToken.token;
            }else{
                //do nothing with rejected with 401 Unauthorized error
            }
        }];
        $httpProvider.interceptors.push('jwtInterceptor');

        $translateProvider.useStaticFilesLoader({
            prefix: 'dataset/languages/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('zh_CN');
        $translateProvider.fallbackLanguage('zh_EN');

        $ionicConfigProvider.tabs.position("bottom"); //Places them at the bottom for all OS
        $ionicConfigProvider.tabs.style("standard"); //Makes them all look the same across all OS

        $stateProvider
            .state('blade', {
                url: "/blade",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'BladeController'
            })

            .state('blade.notes', {
                url: "/notes",
                views: {
                    'menuContent': {
                        templateUrl: "templates/notes/notes.html",
                        controller: 'NotesController'
                    }
                }
            })

            .state('blade.note', {
                url: "/note/:id",
                views: {
                    'menuContent': {
                        templateUrl: "templates/notes/noteDetail.html",
                        controller: 'NoteDetailController'
                    }
                }
            })

            .state('blade.reminders', {
                url: "/reminders",
                views: {
                    'menuContent': {
                        templateUrl: "templates/notes/reminders.html",
                        controller: 'RemindersController'
                    }
                }
            })
            .state('blade.archive', {
                url: "/archive",
                views: {
                    'menuContent': {
                        templateUrl: "templates/notes/archive.html",
                        controller: 'RemindersController'
                    }
                }
            })
            .state('blade.trash', {
                url: "/trash",
                views: {
                    'menuContent': {
                        templateUrl: "templates/notes/trash.html",
                        controller: 'RemindersController'
                    }
                }
            })
            .state('blade.account', {
                url: "/account",
                views: {
                    'menuContent': {
                        templateUrl: "templates/account/account.html",
                        controller: 'AccountController'
                    }
                }
            })
            .state('blade.signIn', {
                url: "/account/signIn",
                views: {
                    'menuContent': {
                        templateUrl: "templates/account/signIn.html",
                        controller: 'AccountController'
                    }
                }
            })
            .state('blade.signUp', {
                url: "/account/signUp",
                views: {
                    'menuContent': {
                        templateUrl: "templates/account/signUp.html",
                        controller: 'AccountController'
                    }
                }
            })
            .state('blade.settings', {
                url: "/account/settings",
                views: {
                    'menuContent': {
                        templateUrl: "templates/account/settings.html",
                        controller: 'AccountController'
                    }
                }
            });
// if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/blade/notes');
    })

    .constant('$ionicLoadingConfig', {
        template: 'Default Loading Template...'
    });